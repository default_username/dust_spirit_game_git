﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHandler : MonoBehaviour
{
    public float rotationSpeed;
    public float upForce;
    private Rigidbody2D rb2d;
    private bool canJump = false;
    private GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        gameController = GameObject.Find("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameController.instance.Life != 0)
        {
            if (Input.GetKeyDown("space") && canJump)
            {
                rb2d.velocity = Vector2.zero;
                rb2d.AddForce(new Vector2(0, upForce));
                canJump = false;
            }
        }
        transform.Find("Body").Rotate(0, 0, Time.deltaTime * gameController.GlobalSpeed * rotationSpeed);
    }
    void OnCollisionEnter2D(Collision2D other) 
    {
        canJump = true;
    }
}
