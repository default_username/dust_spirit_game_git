﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeBox : MonoBehaviour
{
    private GameObject[] lifes;
    private int currentLife = 0;
    public Camera cam;

    
    void Start()
    {

        lifes = new GameObject[GameController.instance.maxLife];
        var LifePrefab = Resources.Load("LifeShown");
        
        Vector3 pos = cam.ScreenToWorldPoint(new Vector3(25, 5));  // ¯\_(ツ)_/¯              
        pos.z = -0.2f;

        for (int i = 0; i < GameController.instance.maxLife; i++)
        {
            lifes[i] = (GameObject)Instantiate(LifePrefab, transform);
            lifes[i].transform.parent = null;

            pos.x += 0.8f;
            lifes[i].GetComponent<RectTransform>().position = pos;
        }
        EventManager.OnLifeGained += AddLife;
        EventManager.OnLifeLost += SubstractLife;
    }

    
    void Update()
    {
        
    }

    void AddLife()
    {
        lifes[currentLife % GameController.instance.maxLife].GetComponent<Animator>().SetBool("Active", true);
        currentLife++;        
    }

    void SubstractLife()
    {
        currentLife--;
        lifes[currentLife].GetComponent<Animator>().SetBool("Active", false);
    }

    void OnDisable()
    {
        EventManager.OnLifeGained -= AddLife;
        EventManager.OnLifeLost -= SubstractLife;
    }
}
