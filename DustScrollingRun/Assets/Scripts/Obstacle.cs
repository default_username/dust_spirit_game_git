﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    private Animator anim;
    private bool collisionHappened = false;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if((other.GetComponent<PlayerHandler>() != null) && (!collisionHappened))
        {
            GameController.instance.WentThroughObstacle();
            anim.SetTrigger("Hit");
            collisionHappened = true;
            //Debug.Log("Collision with " + this.ToString());
        }
    }

    public void ResetObstacle()
    {
        //Debug.Log("Reset called on" + this.ToString());
        anim.SetTrigger("Reset");
        collisionHappened = false;
    }
}
