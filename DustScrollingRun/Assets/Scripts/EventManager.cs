﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    // Start is called before the first frame update
    public delegate void LifeGainedAction();
    public static event LifeGainedAction OnLifeGained;
    
    public static void LifeGained()
    {
        OnLifeGained?.Invoke();
    }

    public delegate void LifeLostAction();
    public static event LifeLostAction OnLifeLost;

    public static void LifeLost()
    {
        OnLifeLost?.Invoke();
    }
}
