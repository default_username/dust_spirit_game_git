﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour
{
    private Vector2 defaultPosition = new Vector2(10f, -10f);
    private bool isOffScreen = true;
    private float lastAppearanceTime;
    public float secondsBeforeNextLife = 0;

    private bool canAppear
    {
        get
        {
            return (GameController.instance.Life < GameController.instance.maxLife) && (Time.time - lastAppearanceTime >= secondsBeforeNextLife) && isOffScreen;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        lastAppearanceTime = Time.time;
    }

    
    void Update()
    {
        if (transform.position.x <= -15f && transform.position.x >= -20f)
        {
            isOffScreen = true;
            transform.parent = null;
        }

        if (canAppear)
        {            
            if(transform.parent == null)
            {
                transform.SetParent(GameController.instance.GetComponent<ObstaclePool>().currentObstacle.transform, false);
                switch((int)(Random.Range(0,100)%2))
                {
                    case 0:
                        {
                            transform.localPosition = new Vector3(0, 3f);
                            break;
                        }
                    case 1:
                        {
                            transform.localPosition = new Vector3(5f, 0);
                            break;
                        }
                    default:
                        {
                            transform.localPosition = new Vector3(0, 0);
                            break;
                        }
                }
                lastAppearanceTime = Time.time;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerHandler>() != null)
        {
            GameController.instance.GotLife();
        }
        transform.parent = null;
        transform.position = defaultPosition;
        isOffScreen = true;
    }

    
}
