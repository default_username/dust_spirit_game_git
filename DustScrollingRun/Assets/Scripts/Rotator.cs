﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Rotator : MonoBehaviour
{
    public float rotationSpeed;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //GetComponent<Transform>().Rotation.Z += 0.1f * rotationSpeed;
        Transform transform = GetComponent<Transform>();
        transform.Rotate(0, 0, Time.deltaTime * 100 * rotationSpeed);
    }
}
