﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroller : MonoBehaviour
{
    public float ScrollSpeed;
    private GameController GameController;
    private Vector2 savedOffset;
    private Renderer renderer;
    private float savedX = 0f;
    private float savedTime;
    
    void Start () {
        renderer = GetComponent<Renderer>();
        savedOffset = renderer.sharedMaterial.GetTextureOffset ("_MainTex");
        GameController = GameObject.Find("GameController").GetComponent<GameController>();
        savedTime = Time.time;
    }

    void Update () {
        savedX = (savedX + /*(Time.time - savedTime)*/ Time.deltaTime * ScrollSpeed * GameController.GlobalSpeed);
        savedTime = Time.time;
        //Debug.Log("Saved Time:" + savedTime);
        float x = Mathf.Repeat(savedX, 1);
        //float x = Mathf.Repeat (Time.time * ScrollSpeed * GameController.GlobalSpeed, 1);
        Vector2 offset = new Vector2 (x, savedOffset.y);
        renderer.sharedMaterial.SetTextureOffset ("_MainTex", offset);
    }

    void OnDisable () {
        renderer.sharedMaterial.SetTextureOffset ("_MainTex", savedOffset);
    }
}
