﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclePool : MonoBehaviour
{
    private int poolSize;
    public int prefabsPerType;
    public Object[] obstaclePrefabs; //prefabs to load from resources
    private GameObject[] obstacles; //the pool itself
    public GameObject currentObstacle;

    public float spawnRate; //pause between spawns
    public float spawnRateVariation; 
    private float nextSpawn;
    private Vector2 obstaclePoolPosition = new Vector2(10f, -10f);
    private float timeSinceObstacleSpawned = 0f;
   // private float spawnXPosition = 12f;
   // private float spawnYPosition = 0f;
    public Vector2 spawnPosition;// = new Vector2(12f, 0f);
    private int currentObstacleIndex = 0;
    
    // Start is called before the first frame update
    void Start()
    {        
        obstaclePrefabs = (Object[]) Resources.LoadAll("Bushes");
        poolSize = obstaclePrefabs.Length * prefabsPerType;
        obstacles = new GameObject[poolSize];

        for (int i = 0; i < obstaclePrefabs.Length; i++)
        {
            for (int j = 0; j < prefabsPerType; j++)
            {
                obstacles[i*prefabsPerType + j] = (GameObject)Instantiate(obstaclePrefabs[j], obstaclePoolPosition, Quaternion.identity);
            }
        }
        nextSpawn = Random.Range(spawnRate, spawnRate + spawnRateVariation);
        currentObstacle = obstacles[currentObstacleIndex];
    }

    // Update is called once per frame
    void Update()
    {
        timeSinceObstacleSpawned += Time.deltaTime;
        if(!(GameController.instance.gameOver) && timeSinceObstacleSpawned >= nextSpawn)
        {
            timeSinceObstacleSpawned = 0f;
            nextSpawn = Random.Range(spawnRate, spawnRate + spawnRateVariation);            
            currentObstacle.transform.position = spawnPosition;            
            currentObstacleIndex++;
            currentObstacleIndex = currentObstacleIndex % poolSize;
            currentObstacle = obstacles[currentObstacleIndex];
            obstacles[(currentObstacleIndex + poolSize/2) % poolSize].GetComponent<Obstacle>().ResetObstacle();
        }
    }
}
