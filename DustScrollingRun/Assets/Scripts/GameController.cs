﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public float DefaultSpeed;
    public float GlobalSpeed;
    public int DefaultLife;
    public int Life;
    public int maxLife;
    public bool gameOver = false;
    public float slowDownFactor = 0.03f;

    private float distanceTraveled = 0;

    public GameObject GameOverUI;
    public GameObject ScoreUI;
    public GameObject GameStartUI;
    private Text scoreText;
    private static bool gameStarted = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        FillLives();
        if(!gameStarted)
            Pause();       
        if (gameStarted)
            GameStartUI.SetActive(false);
        scoreText = ScoreUI.GetComponent<Text>();
        Debug.Log(scoreText);
    }

    void Update()
    {
        if(!gameStarted && Input.GetKeyDown("space"))
        {
            GameStartUI.SetActive(false);
            UnPause();
            gameStarted = true;            
        }
        distanceTraveled += GlobalSpeed * Time.timeScale;
        
        scoreText.text = (distanceTraveled/1000).ToString("n1") + " m";//format to 2 decimal places
        if (gameOver)
        {
            GlobalSpeed = Math.Max(GlobalSpeed - slowDownFactor, 0);           
            GameOverUI.SetActive(true);
        }

        if(gameOver && Input.GetKeyDown("space"))
        {
            Debug.Log("going to call unpause");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            FillLives();              
            UnPause();
            Debug.Log("called unpause");
            GlobalSpeed = DefaultSpeed;
            gameOver = false;
            distanceTraveled = 0;
        }
      
    }

    private void Pause()
    {
        Time.timeScale = 0;
    }

    void UnPause()
    {
        Time.timeScale = 1;
    }

    public void FillLives()
    {
        Life = DefaultLife;
        for (int i = 0; i < Life; i++)
        {
            EventManager.LifeGained();
        }
    }

    public void WentThroughObstacle()
    {
        Life--;
        EventManager.LifeLost();
        if(Life <= 0)
        {
            gameOver = true;
        }

    }

    public void GotLife()
    {
        Life++;
        EventManager.LifeGained();
    }
}
